﻿using CNTK;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LeapLearning
{
    class LeapNetwork
    {
        const int batchSize = 50;
        const int epochCount = 1000;

        Variable x;
        Function y;

        public LeapNetwork(int hiddenNeuronCount, int hiddenLayerCount, DataSet dataSet)
        {
            int[] layers = new int[hiddenLayerCount +2];
            layers[0] = DataSet.InputSize;
            layers[hiddenLayerCount + 1] = dataSet.OutputSize;
            for (int i = 0; i < hiddenLayerCount; i++)
            {
                layers[i + 1] = hiddenNeuronCount;
            }
            x = Variable.InputVariable(new int[] { layers[0] }, DataType.Float, "x");
            Function lastLayer = x;
            for (int i = 0; i < layers.Length - 1; i++)
            {
                Parameter weight = new Parameter(new int[] { layers[i + 1], layers[i] }, DataType.Float, CNTKLib.GlorotNormalInitializer());
                Parameter bias = new Parameter(new int[] { layers[i + 1] }, DataType.Float, CNTKLib.GlorotNormalInitializer());
                Function plus = CNTKLib.Plus(CNTKLib.Times(weight, lastLayer), bias);
                if (i != layers.Length - 2)
                    lastLayer = CNTKLib.Sigmoid(plus);
                else
                    lastLayer = CNTKLib.Softmax(plus);
            }
            y = lastLayer;
        }
        public void Train(DataSet dataSet)
        {
            Variable yt = Variable.InputVariable(new int[] { dataSet.OutputSize }, DataType.Float);
            Function loss = CNTKLib.CrossEntropyWithSoftmax(y, yt);
            Function err = CNTKLib.ClassificationError(y, yt);

            Learner learner = CNTKLib.SGDLearner(new ParameterVector(y.Parameters().ToArray()), new TrainingParameterScheduleDouble(1.0, batchSize));
            Trainer trainer = Trainer.CreateTrainer(y, loss, err, new List<Learner>() { learner });
            Console.WriteLine("Num\tLoss\tAccuracy");
            for (int epochI = 0; epochI <= epochCount; epochI++)
            {
                double sumLoss = 0;
                double sumError = 0;

                for (int batchI = 0; batchI < dataSet.Count / batchSize; batchI++)
                {
                    Data next = dataSet.LoadNext();
                    Value x_value = Value.CreateBatch(x.Shape, next.input, DeviceDescriptor.CPUDevice);
                    Value yt_value = Value.CreateBatch(yt.Shape, next.outnum, DeviceDescriptor.CPUDevice);
                    var inputDataMap = new Dictionary<Variable, Value>()
                    {
                        { x, x_value },
                        { yt, yt_value }
                    };

                    trainer.TrainMinibatch(inputDataMap, false, DeviceDescriptor.CPUDevice);
                    sumLoss += trainer.PreviousMinibatchLossAverage() * trainer.PreviousMinibatchSampleCount();
                    sumError += trainer.PreviousMinibatchEvaluationAverage() * trainer.PreviousMinibatchSampleCount();
                }
                Console.WriteLine(string.Format("{0}\t{1:0.0000}\t{2:0.0000}", epochI, sumLoss / dataSet.Count, 1.0 - sumError / dataSet.Count));
            }
        }
        public void Evaluate(DataSet dataSet, out double lossValue, out double accValue)
        {
            Variable yt = Variable.InputVariable(new int[] { dataSet.OutputSize }, DataType.Float);
            Function loss = CNTKLib.CrossEntropyWithSoftmax(y, yt);
            Function err = CNTKLib.ClassificationError(y, yt);
            Evaluator evaluator_loss = CNTKLib.CreateEvaluator(loss);
            Evaluator evaluator_err = CNTKLib.CreateEvaluator(err);

            double sumEval = 0;
            double sumLoss = 0;
            for (int batchI = 0; batchI < dataSet.Count / batchSize; batchI++)
            {
                Data next = dataSet.LoadNext();
                Value x_value = Value.CreateBatch(x.Shape, next.input, DeviceDescriptor.CPUDevice);
                Value yt_value = Value.CreateBatch(yt.Shape, next.outnum, DeviceDescriptor.CPUDevice);
                var inputDataMap = new UnorderedMapVariableValuePtr()
                {
                    { x, x_value },
                    { yt, yt_value }
                };

                sumLoss += evaluator_loss.TestMinibatch(inputDataMap) * batchSize;
                sumEval += evaluator_err.TestMinibatch(inputDataMap) * batchSize;
            }
            lossValue = sumLoss / dataSet.Count;
            accValue = 1 - sumEval / dataSet.Count;
        }
        public void PredictionTest(DataSet dataSet, int testNum)
        {
            dataSet.Shuffle();
            int predP = 0;
            for (int i = 0; i < testNum; i++)
            {
                Data next = dataSet.LoadNext();
                float[] p = Prediction(next);
                if (next.output == Data.getOutstring(p, dataSet.tagSet.ToArray()))
                {
                    Console.Write(string.Format("{0}: ++++ ", i + 1));
                    predP++;
                } else {
                    Console.Write(string.Format("{0}: ---- ", i + 1));
                }
                Console.WriteLine(string.Format("{0} --> {1}", next.output, Data.getOutstring(p, dataSet.tagSet.ToArray())));
            }
            float acc = (float)predP / testNum;
            Console.WriteLine(string.Format("Correct: {0}\tIncorrect: {1}\tAccuracy: {2:0.00}%",predP, testNum-predP, acc*100));
        }
        public float[] Prediction(Data data)
        {
            var inputDataMap = new Dictionary<Variable, Value>() { { x, Value.CreateBatch(x.Shape, data.input, DeviceDescriptor.CPUDevice) } };
            var outputDataMap = new Dictionary<Variable, Value>() { { y, null } };
            y.Evaluate(inputDataMap, outputDataMap, DeviceDescriptor.CPUDevice);
            return outputDataMap[y].GetDenseData<float>(y)[0].ToArray();
        }
        public void Save()
        {
            y.Save(@"..\Samples\leapnet.model");
        }
    }
}
