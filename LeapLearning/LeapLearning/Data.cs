﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LeapLearning
{
    class Data
    {
        public float[] input;
        public string output;
        public float[] outnum;
        public Data(byte[] rawdata, string tag, string[] tagSet)
        {
            input = new float[rawdata.Length/4];
            loadInput(rawdata);
            output = tag;
            outnum = getOutnum(output, tagSet);
        }
        void loadInput(byte[] rawdata)
        {
            for (int i = 0; i < rawdata.Length; i = i+4)
            {
                input[i/4] = BitConverter.ToSingle(rawdata, i);
            }
        }
        public static float[] getOutnum(string output, string[] tagSet)
        {
            float[] outnum = new float[tagSet.Length];
            outnum[Array.IndexOf(tagSet, output)] = 1;
            return outnum;
        }
        public static string getOutstring(float[] outnum, string[] tagSet)
        {
            int maxI = 0;
            for (int i = 0; i < outnum.Length; i++)
            {
                if (outnum[i] > outnum[maxI])
                {
                    maxI = i;
                }
            }
            return tagSet[maxI];
        }
    }
}
