﻿using System;

namespace LeapLearning
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Train file name without _samples.dat/_tags.dat (empty = Train):\t");
            string trainfile = Console.ReadLine();
            if (trainfile == string.Empty){ trainfile = "Train"; }
            Console.Write("Test file name without _samples.dat/_tags.dat (empty = Test):\t");
            string testfile = Console.ReadLine();
            if (testfile == string.Empty) { testfile = "Test"; }
            Console.Write("Hidden Layer count (recommended 2):\t");
            int hiddenLayerCount = int.Parse(Console.ReadLine());
            Console.Write("Hidden Neuron count in each layer (recommended 300):\t");
            int hiddenNeuronCount = int.Parse(Console.ReadLine());

            DataSet trainDS = new DataSet(@"..\Samples\" + trainfile + "_samples.dat", @"..\Samples\" + trainfile + "_tags.dat");
            DataSet testDS = new DataSet(@"..\Samples\" + testfile + "_samples.dat", @"..\Samples\" + testfile + "_tags.dat");

            LeapNetwork neuralNetwork = new LeapNetwork(hiddenNeuronCount, hiddenLayerCount, trainDS);
            neuralNetwork.Train(trainDS);
            neuralNetwork.PredictionTest(testDS, 100);
            neuralNetwork.Evaluate(trainDS, out double trainLoss, out double trainAcc);
            neuralNetwork.Evaluate(testDS, out double testLoss, out double testAcc);
            Console.WriteLine(string.Format("Final evaluation:\n\tHidden Neurons: {0}\n\tHidden Layers: {1}\n\tTrain loss: {2:0.0000}\n\tTrain Accuracy: {3:0.0000}\n\tTest Loss: {4:0.0000}\n\tTest Accuracy: {5:0.0000}", hiddenNeuronCount, hiddenLayerCount, trainLoss, trainAcc, testLoss, testAcc));
            Console.WriteLine("Save? (Y/N)");
            char c = (char)Console.Read();
            if (c == 'Y' || c == 'y')
            {
                neuralNetwork.Save();
                Console.WriteLine("Saved.");
            }
            else
                Console.WriteLine("Disposed.");
        }
    }
}
