﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace LeapLearning
{
    class DataSet
    {
        //2160 = (5 fingers + 1 palm) * 3 dimensions * 60 fps * 2 sec
        public const int InputSize = 2160;
        //8640 = (5 fingers + 1 palm) * 3 dimensions * 60 fps * 2 sec * 4 byte/float
        const int BufferSize = InputSize * 4;
        private int outputSize = 5;
        public int OutputSize { get { return outputSize; } }
        int[] order;
        int nextI = 0;
        public int Count { get; set; }
        Random rnd = new Random();
        FileStream sampleFile;
        string[] tags;
        public SortedSet<string> tagSet = new SortedSet<string>();

        public DataSet(string sample_path, string tag_path)
        {
            sampleFile = File.OpenRead(sample_path);
            string[] buff = File.ReadAllText(tag_path).Split('$');
            tags = buff.Take(buff.Length - 1).ToArray();
            LoadTagSet(tags);
            outputSize = tagSet.Count;
            Count = tags.Length;
            order = new int[Count]; //the order of the samples to load
            Shuffle(); //loads order first
        }

        //read data in 8640 byte chunks
        public Data LoadNext()
        {
            if (nextI < Count)
            {
                byte[] sampleBuffer = new byte[BufferSize];
                sampleFile.Position = order[nextI] * BufferSize;
                sampleFile.Read(sampleBuffer, 0, BufferSize);
                Data next = new Data(sampleBuffer, tags[order[nextI]], tagSet.ToArray());
                nextI++;
                Normalize(next);
                return next;
            }
            else
            {
                Shuffle();
                nextI = 0;
                return LoadNext();
            }
        }
        void Normalize(Data data)
        {
            for (int i = 0; i < data.input.Length; i = i + 3)
            {
                data.input[i] = (data.input[i] + 30) / 60; //x
                data.input[i + 1] = data.input[i + 1] / 30; //y
                data.input[i + 2] = (data.input[i + 2] + 30) / 60; //z
            }
        }

        //reorder the numbers in the order array
        public void Shuffle()
        {
            if (order[0] == 0 && order[1] == 0) //empty array: first run
            {
                for (int i = 0; i < Count; i++)
                {
                    order[i] = i;
                }
            }
            int x, y, buff;
            for (int i = 0; i < Count; i++)
            {
                x = rnd.Next(Count);
                y = rnd.Next(Count);
                if (x != y)
                {
                    buff = order[x];
                    order[x] = order[y];
                    order[y] = buff;
                }
            }
        }
        void LoadTagSet(string[] tags)
        {
            foreach (var tag in tags)
            {
                tagSet.Add(tag);
            }
        }
    }
}
