﻿using System.IO;

namespace LeapSample
{
    class MotionSample
    {
        float[] buffer;
        public float[] Buffer { get { return buffer; } }
        int bc = 0;
        public int Bc { get { return bc; } }
        string tag = "tag";
        //length in seconds
        public MotionSample(string tag, int length)
        {
            buffer = new float[length * 60 * 6 * 3]; //60 fps * 6 * 3 coordinates
            this.tag = tag;
        }
        public void addPosition(Position position)
        {
            if (bc < buffer.Length - 18)
            {
                buffer[bc] = position.Palm.x;
                buffer[bc+1] = position.Palm.y;
                buffer[bc+2] = position.Palm.z;
                bc += 3;
                for (int i = 0; i < 5; i++)
                {
                    buffer[bc] = position.Fingers[i].x;
                    buffer[bc + 1] = position.Fingers[i].y;
                    buffer[bc + 2] = position.Fingers[i].z;
                    bc += 3;
                }
            }
        }
        public void SaveToFile(string motionpath, string tagpath)
        {
            using (var stream = new FileStream(motionpath, FileMode.Append))
            {
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    foreach (float value in buffer)
                    {
                        writer.Write(value);
                    }
                }
            }
            using (StreamWriter writer = File.AppendText(tagpath))
            {
                writer.Write(tag + "$");
            }
        }
    }
}
