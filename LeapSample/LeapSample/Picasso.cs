﻿using System.Collections.Generic;
using System.Drawing;

namespace LeapSample
{
    class Picasso
    {
        Bitmap bmp;
        int Width = 500, Height = 500, offset = 250;
        Pen p;
        SolidBrush bgcolor, handcolor;
        Rectangle bg;
        Rectangle finger;
        Rectangle palm;
        
        public int LastDraw { get; set; }
        public Picasso(int imgW, int imgH)
        {
            Width = imgW;
            Height = imgH;
            bmp = new Bitmap(imgW, imgH);
            p = new Pen(Color.LightBlue, 1);
            bgcolor = new SolidBrush(Color.Black);
            handcolor = new SolidBrush(Color.LightBlue);
            bg = new Rectangle(0, 0, Width, Height);
            finger = new Rectangle(300, 250, 10, 10);
            palm = new Rectangle(300, 250, 50, 50);
        }
        public Bitmap pleaseDrawThis(Position positions)
        {
            var g = Graphics.FromImage(bmp);
            g.FillRectangle(bgcolor, bg);
            palm.Width = 200-(int)(50 * positions.Palm.y / 100);
            palm.Height = palm.Width;
            palm.X = (int)positions.Palm.x + offset;
            palm.Y = (int)positions.Palm.z + offset;
            g.DrawEllipse(p, palm);
            if (positions.Fingers.Count!= 0)
            {
                for (int i = 0; i < 5; i++)
                {
                    finger.Width = 40-(int)(10 * positions.Fingers[i].y/100);
                    finger.Height = finger.Width;
                    finger.X = (int)positions.Fingers[i].x + offset;
                    finger.Y = (int)positions.Fingers[i].z + offset;
                    g.DrawEllipse(p, finger);
                    g.DrawLine(p, new Point(finger.X + finger.Width/2, finger.Y + finger.Height/2), new Point(palm.X + palm.Width / 2, palm.Y + palm.Height / 2));
                }
            }
            return bmp;
        }
        public Bitmap clearCanvas()
        {
            var g = Graphics.FromImage(bmp);
            g.FillRectangle(bgcolor, bg);
            return bmp;
        }
    }
}