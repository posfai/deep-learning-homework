﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using CNTK;

namespace LeapSample
{
    class LeapEvaluator
    {
        Variable x;
        Function y;
        SortedSet<string> tagSet;
        public LeapEvaluator(string filename, string tagPath)
        {
            y = Function.Load(filename, DeviceDescriptor.CPUDevice);
            x = y.Arguments.First(x => x.Name == "x");
            tagSet = new SortedSet<string>();
            LoadTagSet(tagPath);
        }
        void LoadTagSet(string tag_path)
        {
            string[] buff = File.ReadAllText(tag_path).Split('$');
            foreach (var tag in buff.Take(buff.Length - 1))
            {
                tagSet.Add(tag);
            }
        }
        public string Prediction(float[] data)
        {
            var inputDataMap = new Dictionary<Variable, Value>() { { x, Value.CreateBatch(x.Shape, data, DeviceDescriptor.CPUDevice) } };
            var outputDataMap = new Dictionary<Variable, Value>() { { y, null } };
            y.Evaluate(inputDataMap, outputDataMap, DeviceDescriptor.CPUDevice);
            return getOutstring(outputDataMap[y].GetDenseData<float>(y)[0].ToArray());
        }
        string getOutstring(float[] outnum)
        {
            int maxI = 0;
            for (int i = 0; i < outnum.Length; i++)
            {
                if (outnum[i] > outnum[maxI])
                {
                    maxI = i;
                }
            }
            return tagSet.ElementAt(maxI);
        }
    }
}
