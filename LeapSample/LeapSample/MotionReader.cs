﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Leap;

namespace LeapSample
{
    class MotionReader
    {
        public Controller controller { get; }
        public MotionReader()
        {
            controller = new Controller();
            controller.Connect += OnServiceConnect;
            controller.Device += OnConnect;
            controller.FrameReady += OnFrame;
        }
        ~MotionReader()
        {
            controller.Dispose();
        }

        #nullable enable
        public Position? getPositions()
        {
            Frame frame = controller.Frame();
            //Trace.WriteLine("Num of hands: " + frame.Hands.Count);
            if (frame.Hands.Count == 1)
            {
                Position p = new Position();
                Hand hand = frame.Hands[0];
                p.Palm = hand.PalmPosition;
                for (int i = 0; i < 5; i++)
                {
                    p.Fingers.Add(hand.Fingers[i].TipPosition);
                }
                return p;
            }
            else
            {
                Position test = new Position();
                test.Palm = new Vector(0, 0, 0);
                return test;
            }
        }
        public void OnServiceConnect(object sender, ConnectionEventArgs args)
        {
            Trace.WriteLine("Service Connected");
        }

        public void OnConnect(object sender, DeviceEventArgs args)
        {
            Trace.WriteLine("Connected");
        }

        public void OnFrame(object sender, FrameEventArgs args)
        {
            //Trace.WriteLine("Frame Available.");
        }
    }
}
