﻿using Leap;
using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace LeapSample
{
    class ViewModel : Bindable
    {
        public ICommand StartCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        Picasso Leonardo;
        MotionReader motionReader;
        BitmapImage handpic;
        public int Saves { get; set; }
        public string Info { get; set; }
        public string Info2 { get; set; }
        public string Info3 { get; set; }
        public string FileName { get; set; }
        public string Tag { get; set; }
        public string Evaluation { get; set; }
        bool samplemode = true;
        public bool SampleMode
        {
            get { return samplemode; }
            set
            {
                samplemode = value; 
                if (!samplemode)
                {
                    LoadNN();
                }
                OnPropertyChange(nameof(EvalMode));
            } 
        }
        public bool EvalMode { get { return !samplemode; } set { samplemode = !value; } }
        public BitmapImage HandPic { get { return handpic; } }
        MotionSample currentSample;
        bool REC = false;
        bool PREDICTION = false;
        bool PLAYBACK = false;
        bool DELETE = false;
        int SampleLength = 2; //sec
        LeapEvaluator predictor;

        DateTime timer;

        public ViewModel()
        {
            Leonardo = new Picasso(500, 500);
            motionReader = new MotionReader();
            StartCommand = new RelayCommand(StartMethod, t => !Tag.Contains("$"));
            DeleteCommand = new RelayCommand(DeleteMethod, t => Saves > 0);
            motionReader.controller.FrameReady += OnFrame;
            Info = "Start: save previous and start next";
            Info2 = "Filename to save data:";
            Info3 = "Select mode";
            Tag = "Tag";
            OnPropertyChange(nameof(Info));
            OnPropertyChange(nameof(Tag));
            ClearCanvas();
        }
        public void StartMethod(object o)
        {
            if (SampleMode)
                StartRecord();
            else
                StartEvaluation();
        }
        void StartRecord()
        {
            if (Saves > 0 && currentSample != null && !DELETE)
            {
                currentSample.SaveToFile(@"..\Samples\" + FileName + "_samples.dat", @"..\Samples\" + FileName + "_tags.dat");
            }
            else if (DELETE)
            {
                DELETE = false;
            }
            currentSample = new MotionSample(Tag, SampleLength);
            REC = true;
            Info = "Recording sample...";
            Saves++;
            OnPropertyChange(nameof(Info));
            OnPropertyChange(nameof(Saves));
            timer = DateTime.Now;
            new Thread(TimerThread).Start();
        }
        void StartEvaluation()
        {
            currentSample = new MotionSample(Tag, SampleLength);
            REC = true;
            PREDICTION = true;
            Info = "Recording...";
            Evaluation = "Recording...";
            OnPropertyChange(nameof(Info));
            OnPropertyChange(nameof(Evaluation));
            timer = DateTime.Now;
            new Thread(TimerThread).Start();
        }
        void LoadNN()
        {
            if (FileName == null || FileName == string.Empty)
            {
                FileName = "Train";
            }
            try
            {
                predictor = new LeapEvaluator(@"..\Samples\leapnet.model", @"..\Samples\" + FileName + "_tags.dat");
                Info = "Press Start to recognise gesture!";
                OnPropertyChange(nameof(Info));
                Info2 = "Filename to save data:";
                OnPropertyChange(nameof(Info2));
            }
            catch (FileNotFoundException)
            {
                Info = "Tags file was not found!";
                OnPropertyChange(nameof(Info));
                Info2 = "Write tags.dat filename here -->";
                OnPropertyChange(nameof(Info2));
                SampleMode = true;
                OnPropertyChange(nameof(SampleMode));
                OnPropertyChange(nameof(EvalMode));
            }
        }
        public void DeleteMethod(object o)
        {
            DELETE = true;
            Info = "Last sample deleted, record a new one.";
            OnPropertyChange(nameof(Info));
        }
        public void OnFrame(object sender, FrameEventArgs args)
        {
            #nullable enable
            Position? position = motionReader.getPositions();
            if (position != null && !PLAYBACK)
            {
                UpdateBitmap(position);
                if (REC)
                {
                    currentSample.addPosition(position);
                }
            }
        }
        void UpdateBitmap(Position positions)
        {
            try
            {
                Bitmap bmp = Leonardo.pleaseDrawThis(positions);
                handpic = BitmapToImageSource(bmp);
                handpic.Freeze();
                OnPropertyChange(nameof(HandPic));
            }
            catch (InvalidOperationException) { }
        }
        void PlayBack()
        {
            PLAYBACK = true;
            float[] buff = new float[18];
            for (int i = 0; i < currentSample.Buffer.Length; i = i + 18)
            {
                Array.Copy(currentSample.Buffer, i, buff, 0, 18);
                Position pos = new Position(buff);
                UpdateBitmap(pos);
                Thread.Sleep(10); //lil faster than 60fps
            }
            PLAYBACK = false;
            Info = "Done. Press start to record.";
            OnPropertyChange(nameof(Info));
        }
        void TimerThread()
        {
            while (timer.AddSeconds(SampleLength) > DateTime.Now)
            {
                Thread.Sleep(10);
            }
            REC = false;
            Info = "Playing back record.";
            OnPropertyChange(nameof(Info));
            if (PREDICTION)
            {
                PREDICTION = false;
                Evaluation = predictor.Prediction(currentSample.Buffer);
                Tag = Evaluation;
                OnPropertyChange(nameof(Evaluation));
                OnPropertyChange(nameof(Tag));
            }
            PlayBack();
        }
        void ClearCanvas()
        {
            Bitmap bmp = Leonardo.clearCanvas();
            handpic = BitmapToImageSource(bmp);
            handpic.Freeze();
            OnPropertyChange(nameof(HandPic));
        }
        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }
    }
}