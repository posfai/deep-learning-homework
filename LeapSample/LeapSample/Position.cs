﻿using System.Collections.Generic;
using Leap;

namespace LeapSample
{
    class Position
    {
        public Vector Palm { get; set; }
        public List<Vector> Fingers { get; set; }
        public Position()
        {
            Fingers = new List<Vector>();
        }
        public Position(float[] array)
        {
            Fingers = new List<Vector>();
            Palm = new Vector(array[0], array[1], array[2]);
            for (int i = 0; i < 5; i++)
            {
                Fingers.Add(new Vector(array[i*3+3], array[i*3+4], array[i*3+5]));
            }
        }
        //0:little one, 3: index finger
        public float[] getFinger(int n)
        {
            return new float[] { Fingers[n][0], Fingers[n][1], Fingers[n][2] };
        }
    }
}
