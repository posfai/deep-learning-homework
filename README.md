# Deep Learning Homework #

This homework was created in scope of the Deep Learning subject on the University of Óuda.

### Developer ###

* Gergely Pósfai
* VDNFU7
* gergely.posfai@stud.uni-obuda.hu

### Concept ###
The aim of the project is to create a neural network and a data collecting software which are able to recognize hand gestures captured with a Leap Motion Controller.

### Data Collection ###
The data provided by the Leap Motion Controller contains the positions of the palm and the finger tips. This information is enough to classificate a gesture of a user. The device captures data on 60 fps.

The data collecting software records 2 second samples and stores them in a fix sized buffer. The samples are written into a *filename*_samples.dat file and the tags are written into a *filename*_tags.dat file. The tags are separated with specific characters. The size of one sample is: 2sec × 60 fps × (6 × 3 coordinates) × 4 bytes (c# float) = 8640 bytes.

The data collecting software is able to show and playback the recorded sample and makes it easy to record high amount of samples in a short time. It is also able to recognize gestrures after training the neural network.

### Train ###

Before the train it is possible to set the number and the size of the hidden layers (neuron/layer).
The best results were reached during the tests with 2 hidden layers and 300 neuron in each layer. With these settings and 200 sample after a 1000 epoch 50 batch training the avarage accuracy was 88%. It turned out during the manual tests that the least sampled gesture was significantly worse recognized by the network even in case of small difference.
Similar gestures are also often missed in favor of a gesture taught with multiple patterns.

### Manual testing ###
The LeapSample program requires Leap Motion Orion 3.2.1.
